The package provides classes to interact with PlantUML (http://www.plantuml.com/plantuml) from Java.

## PlantUMLURL 

The class can transform a plain, textual PlantUML diagram to a public available 
URL, where everyone can edit and view the diagram as png or svg graphic.

The class provides three static functions:

```java
PlantUMLURL.UML(String content);
PlantUMLURL.SVG(String content);
PlantUMLURL.PNG(String content);
PlantUMLURL.ASCII(String content);
```

For example a plain PlantUML content like

```
@startuml
Bob -> Alice : hello
@enduml
```
will be transformed into 
http://www.plantuml.com/plantuml/uml/U9npA2v9B2efpStXSifFKj2rKt3CoKnELR1Io4ZDoSddSaZDIm5A0W3u2Gp0

or (the SVG image)
http://www.plantuml.com/plantuml/svg/U9npA2v9B2efpStXSifFKj2rKt3CoKnELR1Io4ZDoSddSaZDIm5A0W3u2Gp0

or (the PNG image)
http://www.plantuml.com/plantuml/png/U9npA2v9B2efpStXSifFKj2rKt3CoKnELR1Io4ZDoSddSaZDIm5A0W3u2Gp0

or (the ASCII Art representation)
http://www.plantuml.com/plantuml/txt/U9npA2v9B2efpStXSifFKj2rKt3CoKnELR1Io4ZDoSddSaZDIm5A0W3u2Gp0

respectively.


Instead of the static convenience methods you can also use special, builder like methods:

```java
PlantUMLURL.Create()
    .withBaseUrl("http://www.plantuml.com/plantuml")
    .withUmlContent(content)
    .umlStyle()
    .toString();
```