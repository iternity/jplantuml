package iternity.plantuml;

import java.io.*;
import java.net.URLDecoder;
import java.util.zip.*;

/**
 * Create a Links to a PlantUML from plain PlantUML content.
 * See http://plantuml.com/plantuml for examples.
 *
 * @Author aweisser
 * @Date 30.11.2018.
 */
public class PlantUMLUrl
{

    /**
     * Convenient method to create a self contained PlantUML link http://plantuml.com/plantuml/svg/{content}
     * If you want to use another base url than http://plantuml.com/plantuml please use the public ctor.
     * @param content
     * @return self contained PlantUML link http://plantuml.com/plantuml/svg/{content}
     */
    public static String SVG(String content)
    {
        return PlantUMLUrl.Create()
                .withUmlContent(content)
                .svgStyle()
                .toString();
    }

    /**
     * Convenient method to create a self contained PlantUML link http://plantuml.com/plantuml/png/{content}
     * If you want to use another base url than http://plantuml.com/plantuml please use the public ctor.
     * @param content
     * @return self contained PlantUML link http://plantuml.com/plantuml/png/{content}
     */
    public static String PNG(String content)
    {
        return PlantUMLUrl.Create()
                .withUmlContent(content)
                .pngStyle()
                .toString();
    }

    /**
     * Convenient method to create a self contained PlantUML link http://plantuml.com/plantuml/uml/{content}
     * If you want to use another base url than http://plantuml.com/plantuml please use the public ctor.
     * @param content
     * @return self contained PlantUML link http://plantuml.com/plantuml/uml/{content}
     */
    public static String UML(String content)
    {
        return PlantUMLUrl.Create()
                .withUmlContent(content)
                .umlStyle()
                .toString();
    }

    /**
     * Convenient method to create a self contained PlantUML link http://plantuml.com/plantuml/txt/{content}
     * If you want to use another base url than http://plantuml.com/plantuml please use the public ctor.
     * @param content PlantUML text
     * @return self contained PlantUML link http://plantuml.com/plantuml/txt/{content}
     */
    public static String ASCII(String content)
    {
        return PlantUMLUrl.Create()
                .withUmlContent(content)
                .asciiStyle()
                .toString();
    }

    public static PlantUMLUrl Create()
    {
        return new PlantUMLUrl();
    }

    private String baseUrl = "http://plantuml.com/plantuml";

    private String umlContent = "@startuml\nlicense\n@enduml";

    private String kind = "uml";


    public PlantUMLUrl withBaseUrl(String baseUrl)
    {
        this.baseUrl = baseUrl;
        return this;
    }

    public PlantUMLUrl withUmlContent(String umlContent)
    {
        this.umlContent = umlContent;
        return this;
    }

    public PlantUMLUrl pngStyle() {
        this.kind = "png";
        return this;
    }

    public PlantUMLUrl svgStyle() {
        this.kind = "svg";
        return this;
    }

    public PlantUMLUrl umlStyle() {
        this.kind = "uml";
        return this;
    }

    public PlantUMLUrl asciiStyle() {
        this.kind = "txt";
        return this;
    }

    public String toString()
    {
        String content = this.umlContent;
        try {
            content = URLDecoder.decode(content, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            content = URLDecoder.decode(content);
        }
        byte[] bytes = zipString(content);
        String base64 = encode64(bytes);

        return baseUrl+"/"+kind+"/"+base64;
    }

    private static byte[] zipString(String str)
    {
        try
        {
            ByteArrayOutputStream compressedStream = new ByteArrayOutputStream();
            Writer writer = new OutputStreamWriter(new DeflaterOutputStream(compressedStream));
            writer.write(str);
            writer.close();

            return compressedStream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String encode64(byte[] data)
    {
        String r = "";
        for (int i = 0; i < data.length; i += 3)
        {
            if (i + 2 == data.length)
            {
                r += append3bytes(data[i], data[i + 1], 0);
            }
            else if (i + 1 == data.length)
            {
                r += append3bytes(data[i], 0, 0);
            }
            else
            {
                r += append3bytes(data[i], data[i + 1],
                        data[i + 2]);
            }
        }
        return r;
    }

    private static String append3bytes(int b1, int b2, int b3)
    {
        // Convert to signed bytes
        b1 &= 0xFF;
        b2 &= 0xFF;
        b3 &= 0xFF;

        int c1 = b1 >> 2;
        int c2 = ((b1 & 0x3) << 4) | (b2 >> 4);
        int c3 = ((b2 & 0xF) << 2) | (b3 >> 6);
        int c4 = b3 & 0x3F;
        String r = "";
        r += encode6bit(c1 & 0x3F);
        r += encode6bit(c2 & 0x3F);
        r += encode6bit(c3 & 0x3F);
        r += encode6bit(c4 & 0x3F);
        return r;
    }

    private static char encode6bit(int b)
    {
        if (b < 10)
        {
            return (char)(48 + b);
        }
        b -= 10;
        if (b < 26)
        {
            return (char)(65 + b);
        }
        b -= 26;
        if (b < 26)
        {
            return (char)(97 + b);
        }
        b -= 26;
        if (b == 0) return '-';
        return b == 1 ? '_' : '?';
    }
}