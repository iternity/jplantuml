package iternity.plantuml;

import org.junit.Assert;
import org.junit.Test;

/**
 * @Author aweisser
 * @Date 30.11.2018.
 */
public class PlantUMLUrlTests {

    private static final String ALICE_AND_BOB_SEQUENCE = "@startuml\nBob -> Alice : hello\n@enduml";

    @Test
    public void defaultLicenseDiagram() {
        String url = PlantUMLUrl.Create().toString();
        Assert.assertEquals("http://plantuml.com/plantuml/uml/U9npA2v9B2efpStXoibCJiqhJkLoICrB0V81UzK9UG00", url);
    }

    @Test
    public void bobAndAliceSequenceDiagramAsSVG() {
        String url = PlantUMLUrl.SVG(ALICE_AND_BOB_SEQUENCE);
        Assert.assertEquals("http://plantuml.com/plantuml/svg/U9npA2v9B2efpStXSifFKj2rKt3CoKnELR1Io4ZDoSddSaZDIm5A0W3u2Gp0", url);
    }

    @Test
    public void bobAndAliceSequenceDiagramAsPNG() {
        String url = PlantUMLUrl.PNG(ALICE_AND_BOB_SEQUENCE);
        Assert.assertEquals("http://plantuml.com/plantuml/png/U9npA2v9B2efpStXSifFKj2rKt3CoKnELR1Io4ZDoSddSaZDIm5A0W3u2Gp0", url);
    }

    @Test
    public void bobAndAliceSequenceDiagramAsUML() {
        String url = PlantUMLUrl.UML(ALICE_AND_BOB_SEQUENCE);
        Assert.assertEquals("http://plantuml.com/plantuml/uml/U9npA2v9B2efpStXSifFKj2rKt3CoKnELR1Io4ZDoSddSaZDIm5A0W3u2Gp0", url);
    }

    @Test
    public void bobAndAliceSequenceDiagramAsASCII() {
        String url = PlantUMLUrl.ASCII(ALICE_AND_BOB_SEQUENCE);
        Assert.assertEquals("http://plantuml.com/plantuml/txt/U9npA2v9B2efpStXSifFKj2rKt3CoKnELR1Io4ZDoSddSaZDIm5A0W3u2Gp0", url);
    }

    @Test
    public void bobAndAliceSequenceDiagramWithLocalBaseUrl() {
        String url = PlantUMLUrl.Create().withBaseUrl("http://localhost:8080/plantuml").umlStyle().withUmlContent(ALICE_AND_BOB_SEQUENCE).toString();
        Assert.assertEquals("http://localhost:8080/plantuml/uml/U9npA2v9B2efpStXSifFKj2rKt3CoKnELR1Io4ZDoSddSaZDIm5A0W3u2Gp0", url);
    }
}
